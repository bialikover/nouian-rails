require 'test_helper'

class ProfilesControllerTest < ActionController::TestCase
  setup do
    @profile = profiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:profiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create profile" do
    assert_difference('Profile.count') do
      post :create, profile: { address: @profile.address, city: @profile.city, country: @profile.country, first_name: @profile.first_name, last_name: @profile.last_name, negative_as_buyer: @profile.negative_as_buyer, negative_as_seller: @profile.negative_as_seller, overall_buyer: @profile.overall_buyer, overall_seller: @profile.overall_seller, positive_as_buyer: @profile.positive_as_buyer, positive_as_seller: @profile.positive_as_seller, postal_code: @profile.postal_code, state: @profile.state, telephone: @profile.telephone, total_boughts: @profile.total_boughts, total_sales: @profile.total_sales }
    end

    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should show profile" do
    get :show, id: @profile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @profile
    assert_response :success
  end

  test "should update profile" do
    put :update, id: @profile, profile: { address: @profile.address, city: @profile.city, country: @profile.country, first_name: @profile.first_name, last_name: @profile.last_name, negative_as_buyer: @profile.negative_as_buyer, negative_as_seller: @profile.negative_as_seller, overall_buyer: @profile.overall_buyer, overall_seller: @profile.overall_seller, positive_as_buyer: @profile.positive_as_buyer, positive_as_seller: @profile.positive_as_seller, postal_code: @profile.postal_code, state: @profile.state, telephone: @profile.telephone, total_boughts: @profile.total_boughts, total_sales: @profile.total_sales }
    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should destroy profile" do
    assert_difference('Profile.count', -1) do
      delete :destroy, id: @profile
    end

    assert_redirected_to profiles_path
  end
end
