class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.float :quantity
      t.text :description
      t.float :price
      t.boolean :featured
      t.string :material
      t.string :finished
      t.string :color

      t.timestamps
    end
  end
end
