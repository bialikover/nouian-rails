class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.string :country
      t.string :state
      t.string :city
      t.text :description

      t.timestamps
    end
  end
end
