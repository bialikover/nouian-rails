class CreateProductImages < ActiveRecord::Migration
  def change
    create_table :product_images do |t|
      t.integer :product_id
      t.has_attached_file :product_photo
      t.timestamps
    end
  end
end

