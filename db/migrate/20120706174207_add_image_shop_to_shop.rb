class AddImageShopToShop < ActiveRecord::Migration
  def up
    change_table :shops do |t|
      t.has_attached_file :image_shop
    end
  end

  def down
    drop_attached_file :shops, :image_shop
  end
end
