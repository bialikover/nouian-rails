class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :telephone
      t.string :city
      t.string :state
      t.string :country
      t.string :postal_code
      t.integer :total_boughts
      t.integer :total_sales
      t.integer :positive_as_seller
      t.integer :negative_as_seller
      t.integer :positive_as_buyer
      t.integer :negative_as_buyer
      t.integer :overall_seller
      t.integer :overall_buyer

      t.timestamps
    end
  end
end
