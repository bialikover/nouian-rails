class AddUserIdToProfiles2 < ActiveRecord::Migration
  def change
  	 change_table(:profiles) do |t| 
      ## Confirmable
      t.integer   :user_id
  	end
  end
end
