class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :quantity
      t.float :total
      t.date :made_at
      t.boolean :completed
      t.boolean :buyer_rate
      t.boolean :seller_rate

      t.timestamps
    end
  end
end
