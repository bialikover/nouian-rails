class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :content
      t.time :made_at
      t.text :answer
      t.time :answered_at

      t.timestamps
    end
  end
end
