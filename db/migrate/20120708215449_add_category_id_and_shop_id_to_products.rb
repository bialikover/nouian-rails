class AddCategoryIdAndShopIdToProducts < ActiveRecord::Migration
  def change
  	change_table :products do |t|
      t.integer :shop_id      
    end
  end
end
