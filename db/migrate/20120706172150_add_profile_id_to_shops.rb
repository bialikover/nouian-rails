class AddProfileIdToShops < ActiveRecord::Migration
  def change
  	change_table(:shops) do |t| 
      ## Confirmable
      t.integer   :profile_id
  	end
  end
end
