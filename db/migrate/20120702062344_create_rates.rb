class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.string :value
      t.text :comment
      t.boolean :completed
      t.string :reason

      t.timestamps
    end
  end
end
