class ProductsController < ApplicationController
  before_filter :authenticate_user!, :except =>[:index, :show]
  # GET /products
  # GET /products.json
  def index
    @products = Product.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new

    if has_shop(current_user) && current_user.profile.shop.ammount >= 3
      @product = Product.new
      3.times {@product.product_images.build}      
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @product }
      end
    else
      flash[:notice] = "Imposible crear producto sin tienda y/o credito"
      redirect_to "/panel" 
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
  end

  # POST /products
  # POST /products.json
  def create

    #abort(params.inspect)

    if has_shop(current_user) && current_user.profile.shop.ammount >= 3
      @product = Product.new(params[:product])
      @product.shop_id = current_user.profile.shop.id      
      respond_to do |format|
        if @product.save
          format.html { redirect_to @product, notice: 'Product was successfully created.' }
          format.json { render json: @product, status: :created, location: @product }
        else
          format.html { render action: "new" }
          format.json { render json: @product.errors, status: :unprocessable_entity }
        end      
      end
    else
      flash[:notice] = "Imposible crear producto sin tienda y/o credito"
      redirect_to "/panel" 
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    params[:product][:category_ids] ||= []
    @product = Product.find(params[:id])
    3.times {@product.product_images.build}      
    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end
end
