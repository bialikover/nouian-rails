class ShopsController < ApplicationController
  before_filter :authenticate_user!, :except =>[:index, :show]
  # GET /shops
  # GET /shops.json

  def index
    @shops = Shop.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @shops }
    end
  end

  # GET /shops/1
  # GET /shops/1.json
  def show
    @shop = Shop.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @shop }
    end
  end

  # GET /shops/new
  # GET /shops/new.json
  def new
    if ! has_shop(current_user) && has_profile(current_user)
      @shop = Shop.new
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @shop }
      end
    else
      flash[:notice] = "Imposible crear tienda"
      redirect_to "/panel" 
    end
  end

  # GET /shops/1/edit
  def edit
    @shop = Shop.find(params[:id])
  end

  # POST /shops
  # POST /shops.json
  def create

    if ! has_shop(current_user)
      @shop = Shop.new(params[:shop])
      @shop.profile_id = current_user.profile.id
      respond_to do |format|
        if @shop.save
          format.html { redirect_to @shop, notice: 'Shop was successfully created.' }
          format.json { render json: @shop, status: :created, location: @shop }
        else
          format.html { render action: "new" }
          format.json { render json: @shop.errors, status: :unprocessable_entity }
        end
      end    
    else
      flash[:notice] = "Imposible crear otra tienda"
      redirect_to "/panel" 
    end
  end

  # PUT /shops/1
  # PUT /shops/1.json
  def update
    @shop = Shop.find(params[:id])

    respond_to do |format|
      if @shop.update_attributes(params[:shop])
        format.html { redirect_to @shop, notice: 'Shop was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shops/1
  # DELETE /shops/1.json
  def destroy
    @shop = Shop.find(params[:id])
    @shop.destroy

    respond_to do |format|
      format.html { redirect_to shops_url }
      format.json { head :no_content }
    end
  end
end
