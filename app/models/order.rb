class Order < ActiveRecord::Base
  attr_accessible :buyer_rate, :completed, :made_at, :quantity, :seller_rate, :total
  has_many :products
  #only two rates:
  has_many :rates 
  belongs_to :profile
end
