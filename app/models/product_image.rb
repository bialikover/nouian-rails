class ProductImage < ActiveRecord::Base
  attr_accessible :product_id, :product_photo
  belongs_to :product
  has_attached_file :product_photo, 
  			:styles => { :medium => "300x300>", :thumb => "100x100>" },
  			#:url  => "/assets/shops/:id/:style/:basename.:extension",
  			#:path => ":rails_root/public/assets/shops/:id/:style/:basename.:extension"
  			:storage => :s3,     		
     		:s3_credentials => "#{Rails.root}/config/s3.yml",
     		:path => "/products/:photo_product_id/:style/:basename.:extension"
  
end
