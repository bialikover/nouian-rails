class Product < ActiveRecord::Base
  after_create :decrease_credit
  attr_accessible :color, :description, :featured, :finished, :material, 
  				  :name, :price, :quantity, :category_ids, :product_images_attributes
  #has_many :categories_products
  #has_many :categories, :through => :categories_products
  #accepts_nested_attributes_for :categories_products
  has_and_belongs_to_many :categories
  belongs_to :shop
  has_many :questions
  has_many :product_images
  belongs_to :order
  accepts_nested_attributes_for :product_images
  validates :color, :description, :finished, :material, 
  				  :name, :price, :quantity, :category_ids, :presence => true
  
  validates :name, :length => { :minimum => 3 }
  validates :quantity, :numericality => { :greater_than => 0 , :less_than => 20}
  validates :price, :numericality => { :greater_than => 0 }
  validates :description, :length => { :minimum => 20 }

  #has_many :tags

  def decrease_credit
   self.shop.ammount -= 3 # el credito a descontarse
   if self.featured != 0 && self.shop.ammount > 10
   	self.shop.ammount -= 10
   else
   	self.update_featured_false
   end
   self.shop.save
  end

  def update_featured_false
	self.update_attribute(:featured, false)  	
  end

end
