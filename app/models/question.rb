class Question < ActiveRecord::Base
  attr_accessible :answer, :answered_at, :content, :made_at
  belongs_to :product
  belongs_to :profile
end
