class Profile < ActiveRecord::Base
  before_create :default_values
  attr_accessible :address, :city, :country, :first_name, :last_name, :postal_code, :state, :telephone
  #,:negative_as_buyer, :negative_as_seller, :overall_buyer, :overall_seller, 
  #:positive_as_buyer, :positive_as_seller,  
  #:total_boughts, :total_sales
  belongs_to :user
  has_one :shop 
  has_many :questions
  has_many :orders
  has_many :rates
  validates :address, :city, :country, :first_name, :last_name, :telephone, :state, :presence => true
  validates :first_name, :last_name, :length => { :minimum => 2 }
  validates :postal_code, :telephone, :numericality => true


  def default_values
    self.negative_as_buyer  ||= 0
    self.negative_as_seller ||= 0
    self.positive_as_buyer  ||=  0
    self.positive_as_seller ||=  0
    self.overall_buyer      ||=  100
    self.overall_seller     ||=  100
    self.total_sales        ||=  0
    self.total_boughts      ||=  0    
  end



end
