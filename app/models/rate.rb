class Rate < ActiveRecord::Base
  attr_accessible :comment, :completed, :reason, :value
  belongs_to :order
  belongs_to :profile
end
