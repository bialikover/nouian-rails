class Ability
  include CanCan::Ability
  
  def initialize(user)
    user ||= User.new # guest user

    if user.role? :administrator
      can :manage, :all
    elsif user.role? :seller
      can :manage, Products
    else 
      can :read, :all
    end
  end
end