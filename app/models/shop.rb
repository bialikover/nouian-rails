class Shop < ActiveRecord::Base
  attr_accessible :city, :country, :description, :name, :state, :image_shop
  before_create :default_values
  has_many :products
  belongs_to :profile
  has_attached_file :image_shop, :styles => { :medium => "300x300>", :thumb => "100x100>" },
  			#:url  => "/assets/shops/:id/:style/:basename.:extension",
  			#:path => ":rails_root/public/assets/shops/:id/:style/:basename.:extension"
  			:storage => :s3,     		
     		:s3_credentials => "#{Rails.root}/config/s3.yml",
     		:path => "/shops/:id/:style/:basename.:extension"


  validates :city, :country, :description, :name, :state, :presence => true
  validates :name, :length => { :minimum => 3 }
  validates :description, :length => { :minimum => 20 }
  
  def default_values
  	self.ammount = 50.0 #define la cantidad inicial de credito.
  end


end
