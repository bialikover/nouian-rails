class Category < ActiveRecord::Base
  attr_accessible :description, :name, :parent
  #has_many :categories_products
  #has_many :products, :through => :categories_products
  has_and_belongs_to_many :products
end
